import {createRouter, createWebHistory} from 'vue-router'
import Home from "../pages/Home";
import Authorization from "../pages/Authorization";
import Register from "../pages/Register";
import Password from "../pages/Password";
import Catalog from "../pages/Catalog";
import Bucket from "../pages/Bucket";
import Product from "../pages/Product";

const routes = [
    {
        path: '/',
        name: 'home',
        component: Home
    },
    {
        path: '/auth',
        name: 'auth',
        component: Authorization
    },
    {
        path: '/register',
        name: 'register',
        component: Register
    },
    {
        path: '/password',
        name: 'password',
        component: Password
    },
    {
        path: '/catalog',
        name: 'catalog',
        component: Catalog
    },
    {
        path: '/bucket',
        name: 'bucket',
        component: Bucket
    },
    {
        path: '/product',
        name: 'product',
        component: Product,
    },
]

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes
})

export default router
